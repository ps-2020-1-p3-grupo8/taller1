#include <stdio.h>
#include <stdlib.h>


void main(){
	int filas;
	int columnas;
	int inicioc;
	int iniciof;
	
	printf("Ingrese filas: ");
	scanf("%d", &filas);
	
	printf("Ingrese columnas: ");
	scanf("%d", &columnas);
	
	printf("Inicio columnas: ");
	scanf("%d", &inicioc);
	
	printf("Inicio filas: ");
	scanf("%d", &iniciof);
	
	if(filas<0 || columnas< 0 || inicioc<0 || iniciof<0){
		printf("No aceptamos numeros negativos \n");
		exit(-1);
	}

	
	printf("Tabla de multiplicar\n");
	
	int i;
  	int k;
      	for(k=inicioc;k<=inicioc+columnas-1;k++){
        	printf("\t%i",k);
     	}
  	printf("\n");
	for(i=iniciof;i<=filas+iniciof-1;i++){
    		int j;
    		int valor;
    		printf("%i\t",i);
		for(j=inicioc;j<=inicioc+columnas-1;j++){
      			valor=i*j;
      			printf("%i\t",valor);
    	}
    	printf("\n");
  }
	
	
	
}
